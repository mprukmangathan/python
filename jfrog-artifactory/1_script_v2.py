import os
import re
from bs4 import BeautifulSoup
import requests
import datetime
import boto3
import time
import json
import botocore

def create_instance():
    # Specify the appropriate instance name, default - "windows_2016_sandbox_(today_date)_(current_time)"
    Today_Date = datetime.datetime.today().strftime ('%d_%b_%Y')
    dateTime   = datetime.datetime.now()
    Time       = dateTime.strftime("%H_%M")
    HH         = dateTime.strftime("%H")
    ec2_instance_name = "windows_2016_sandbox_" + Today_Date + "_" + Time
    ec2_re = "windows_2016_sandbox_" + Today_Date
    
    # Get the latest ec2 instance name from s3 bucket 
    S3Bucket = []
    #s3 = boto3.client('s3')
    s3 = boto3.resource('s3')
    my_bucket = s3.Bucket('sandbox-ec2-hostname-testcase1')
    for file in my_bucket.objects.all():
        S3Bucket.append(file.key)
    print(S3Bucket)
    if not S3Bucket:
       print("Bucket is empty")
       run_instances(ec2_instance_name)
    else:
       latest_instance_name = S3Bucket.pop()
       if ec2_re in latest_instance_name:
          print("## Instance with name %s already exist" %latest_instance_name)
       else:
          print("## Validation Success, creating the instance - %s" %ec2_instance_name)
          run_instances(ec2_instance_name)

# creating ssm role for ec2 to run cmd remotely
def create_ssm_role_for_ec2():
    assume_role_policy_document = json.dumps(
      {
          "Version": "2012-10-17",
          "Statement": [
            {
               "Effect": "Allow",
               "Principal": {
                   "Service": "ec2.amazonaws.com"
               },
               "Action": "sts:AssumeRole"
            }
          ]
      }
    )
    client = boto3.client('iam')
    crerol = client.create_role(
       RoleName='role_for_ec2_ssm',
       Description='system manager to run cmd remotely',
       AssumeRolePolicyDocument = assume_role_policy_document
    )
    #print(crerol)
    attpol = client.attach_role_policy(
       RoleName='role_for_ec2_ssm', PolicyArn='arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM'
    )
    #print(attpol)
    creinstpro = client.create_instance_profile(
       InstanceProfileName='role_for_ec2_ssm',
    )
    adrolip = client.add_role_to_instance_profile(
       InstanceProfileName='role_for_ec2_ssm',
       RoleName='role_for_ec2_ssm'
    )
    print("## Role has been created successfully!")

def is_ec2_role_exist():
    client = boto3.client('iam')
    try:
       response = client.get_role(RoleName='role_for_ec2_ssm')
       print("## Role role_for_ec2_ssm already exist")
    except:
       print("## Role role_for_ec2_ssm doesnot exist, hence creating the role..")
       create_ssm_role_for_ec2()

# Specify the appropriate aws region ??
def run_instances(ec2_instance_name):
    is_ec2_role_exist()
    client = boto3.client('ec2', region_name='us-east-1')
    ec2 = boto3.resource('ec2')

    # Update the volume size according to your need ??
    try:
      response = client.run_instances(
         BlockDeviceMappings=[
             {
                 'DeviceName': '/dev/xvda',
                 'Ebs': {
                     'DeleteOnTermination': True,
                     'VolumeSize': 8,
                     'VolumeType': 'gp2'
                 },
             },
         ],
         # update the appropriate imageid (if any custom image) ??
         # please note, the image id will defer based on the region
         ImageId='ami-0947d2ba12ee1ff75',
         # Choose the appropriate instance type ??
         InstanceType='t2.micro',
         # Choose the appropriate keyname ??
         KeyName='ruk_iaac',
         MaxCount=1,
         MinCount=1,
         Monitoring={
            'Enabled': False
         },
         # Attaching SSM IAM role
         IamInstanceProfile={
             'Name': 'role_for_ec2_ssm'
         },
         # Choose the appropriate security group ??
         SecurityGroupIds=[
            'sg-07abb0071ada9dd70',
         ],
         # Add instance tags if needed
         TagSpecifications=[
            {
               'ResourceType': 'instance',
               'Tags': [
                  {
                      'Key': 'Name',
                      'Value': ec2_instance_name
                  },
               ]
            },
         ]
      )
      print("## New instance has been created")
      print("## Writing instance name to s3")
      
      # Writing instance name on s3 for future reference
      s3 = boto3.client('s3')
      s3.put_object(Bucket='sandbox-ec2-hostname-testcase1', Key=ec2_instance_name)
      print("## Process completed, exiting")
    except Exception as e:
      print("## Unable to launch an instance %s" %e)

def lambda_handler(event, context):
    # Get current date - 10-08-2020
    Current_Date = datetime.datetime.today().strftime ('%d-%b-%Y')

    # Listing artifact 'generic-local' files
    # Update the appropriate jfrog artifactory link ??
    ip = os.environ['elasticip']
    url = "http://" + ip + ":8082/artifactory/example-repo-local/"
    response = requests.get(url, auth=('admin','Admin@123'))
    soup = BeautifulSoup(response.text, 'html.parser')
    Mylist = soup.find_all('pre')
    Mylist.pop(0)

    for element in Mylist:
        Mynewlist = str(element).split("\n")
        Mynewlist.pop()
    
    ToDay = "NO"
    Now = "NO"
    for item in Mynewlist:
        Text = item.split("</a>")[0]
        filEname = re.search(r'(?<=href=")[^"]+', Text).group()
        daTe = item.split("</a>")[1]
        datetIme = daTe.split()
        file_creation_date = datetIme[0]
        #file_creation_time = datetIme[1]
        if Current_Date == file_creation_date:
           print("## File %s uploaded on %s" %(filEname,file_creation_date))
           ToDay = "YES"

    print("## ---------------------------------------------------------------------------------------")
    if ToDay == "NO":
       print("## RESULT: There is no files uploaded for today")
       print("## ---------------------------------------------------------------------------------------")
    if ToDay == "YES":
       print("## RESULT: Found Files uploaded for today")
       print("## ---------------------------------------------------------------------------------------")
       print("## validating if any ec2 instance created in last 60 minutes")
       create_instance()
