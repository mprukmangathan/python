import os
import re
from bs4 import BeautifulSoup
import requests
import datetime
import boto3
import time
import json
import botocore

def create_instance():
    # Specify the appropriate instance name, default - "windows_2016_sandbox_(today_date)_(current_time)"
    Today_Date = datetime.datetime.today().strftime ('%d_%b_%Y')
    dateTime   = datetime.datetime.now()
    Time       = dateTime.strftime("%H_%M")
    HH         = dateTime.strftime("%H")
    ec2_instance_name = "windows_2016_sandbox_" + Today_Date + "_" + Time
    ec2_re = "windows_2016_sandbox_" + Today_Date + "_" + HH
    
    # Get the latest ec2 instance name from s3 bucket 
    S3Bucket = []
    s3 = boto3.client('s3')
    for key in s3.list_objects(Bucket='sandbox-ec2-hostname')['Contents']:
        S3Bucket.append(key['Key'])
    latest_instance_name = S3Bucket.pop()
    if ec2_re in latest_instance_name:
       print("## Instance with name %s already exist" %latest_instance_name)
    else:
       print("## Validation Success, creating the instance - %s" %ec2_instance_name)
       run_instances(ec2_instance_name)

# creating ssm role for ec2 to run cmd remotely
def create_ssm_role_for_ec2():
    assume_role_policy_document = json.dumps(
      {
          "Version": "2012-10-17",
          "Statement": [
            {
               "Effect": "Allow",
               "Principal": {
                   "Service": "ec2.amazonaws.com"
               },
               "Action": "sts:AssumeRole"
            }
          ]
      }
    )
    client = boto3.client('iam')
    crerol = client.create_role(
       RoleName='role_for_ec2_ssm',
       Description='system manager to run cmd remotely',
       AssumeRolePolicyDocument = assume_role_policy_document
    )
    #print(crerol)
    attpol = client.attach_role_policy(
       RoleName='role_for_ec2_ssm', PolicyArn='arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM'
    )
    #print(attpol)
    creinstpro = client.create_instance_profile(
       InstanceProfileName='role_for_ec2_ssm',
    )
    adrolip = client.add_role_to_instance_profile(
       InstanceProfileName='role_for_ec2_ssm',
       RoleName='role_for_ec2_ssm'
    )
    print("## Role has been created successfully!")

def is_ec2_role_exist():
    client = boto3.client('iam')
    try:
       response = client.get_role(RoleName='role_for_ec2_ssm')
       print("## Role role_for_ec2_ssm already exist")
    except:
       print("## Role role_for_ec2_ssm doesnot exist, hence creating the role..")
       create_ssm_role_for_ec2()

# Specify the appropriate aws region ??
def run_instances(ec2_instance_name):
    is_ec2_role_exist()
    client = boto3.client('ec2', region_name='us-east-2')
    ec2 = boto3.resource('ec2')
    
    # Update the appropriate github repository url in variable - $url (line - 18)??
    # Update the exact path of extracted repository location in powershell (line 4, 25)
    user_data = '''<powershell>
    if (Test-Path C:\gitrepo\) {
      get-date >> C:\gitrepo\Reboot_log.txt
      powershell "& ""C:\gitrepo\myrepo\*\software\install.ps1"""
    } else {
      mkdir c:\gitrepo
      mkdir c:\gitrepo\myrepo
      cd c:\gitrepo

      $Passwd = "S@ndb0x"
      $secureStringPassword = ($Passwd | ConvertTo-SecureString -AsPlainText -Force)
      New-LocalUser "sandbox" -Password $secureStringPassword -FullName "Sandbox-User" -Description "To read log files"
      Add-LocalGroupMember -Group "Administrators" -Member "sandbox"

      [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
      $down = New-Object System.Net.WebClient
      $url = 'https://bitbucket.org/mprukmangathan/powershell/get/3355b0f03e5b.zip'
      $file = 'C:\gitrepo\myrepo.zip'
      $down.DownloadFile($url,$file)
      $exec = New-Object -com shell.application
      $destination = 'C:\gitrepo\myrepo'
      Expand-Archive -LiteralPath $file -DestinationPath $destination

      powershell "& ""C:\gitrepo\myrepo\*\software\install.ps1"""
    }
 
    if ($? -eq "True") {
       cd c:\gitrepo
       echo "process successfull" > execution_logs.txt
    } else {
       cd c:\gitrepo
       echo "process unsuccessfull"  > execution_logs.txt
    }
    </powershell>
    <persist>true</persist>'''

    # Update the volume size according to your need ??
    try:
      response = client.run_instances(
         BlockDeviceMappings=[
             {
                 'DeviceName': '/dev/xvda',
                 'Ebs': {
                     'DeleteOnTermination': True,
                     'VolumeSize': 8,
                     'VolumeType': 'gp2'
                 },
             },
         ],
         # update the appropriate imageid (if any custom image) ??
         # please note, the image id will defer based on the region
         ImageId='ami-0587770c46124f677',
         # Choose the appropriate instance type ??
         InstanceType='t2.micro',
         # Choose the appropriate keyname ??
         KeyName='ruk2020',
         MaxCount=1,
         MinCount=1,
         Monitoring={
            'Enabled': False
         },
         # Attaching SSM IAM role
         IamInstanceProfile={
             'Name': 'role_for_ec2_ssm'
         },
         # Choose the appropriate security group ??
         SecurityGroupIds=[
            'sg-04a97a6bb4cc46013',
         ],
         # Add instance tags if needed
         TagSpecifications=[
            {
               'ResourceType': 'instance',
               'Tags': [
                  {
                      'Key': 'Name',
                      'Value': ec2_instance_name
                  },
               ]
            },
         ],
         UserData = user_data
      )
      print("## New instance has been created")
      print("## Writing instance name to s3")
      
      # Writing instance name on s3 for future reference
      s3 = boto3.client('s3')
      s3.put_object(Bucket='sandbox-ec2-hostname', Key=ec2_instance_name)
      print("## Process completed, exiting")
    except botocore.exceptions.ClientError as err:
      print("## Unable to launch an instance %s" %err)

def lambda_handler(event, context):
    # Get current date - 10-08-2020
    Current_Date = datetime.datetime.today().strftime ('%d-%b-%Y')

    # Get current time - 11:56
    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M")

    # Getting 10 minutes timeframe
    Ten_minutes = datetime.timedelta(minutes=10)

    # Geeting 10 minutes before time frame (currenttime - 10 minutes)
    Validate_time = now - Ten_minutes
    tem_min_before = Validate_time.strftime("%H:%M")

    # Listing artifact 'generic-local' files
    # Update the appropriate jfrog artifactory link ??
    ip = os.environ['elasticip']
    url = "http://" + ip + ":8082/artifactory/generic-local/"
    response = requests.get(url, auth=('admin','admin@123'))
    soup = BeautifulSoup(response.text, 'html.parser')
    Mylist = soup.find_all('pre')
    Mylist.pop(0)

    for element in Mylist:
        Mynewlist = str(element).split("\n")
        Mynewlist.pop()
    
    ToDay = "NO"
    Now = "NO"
    for item in Mynewlist:
        Text = item.split("</a>")[0]
        filEname = re.search(r'(?<=href=")[^"]+', Text).group()
        daTe = item.split("</a>")[1]
        datetIme = daTe.split()
        file_creation_date = datetIme[0]
        file_creation_time = datetIme[1]
        if Current_Date == file_creation_date:
           print("## File %s uploaded on %s" %(filEname,file_creation_date))
           ToDay = "YES"
           if ((file_creation_time >= tem_min_before) and (file_creation_time <= current_time)):
              print ("## File %s uploaded today at %s" %(filEname, file_creation_time))
              Now = "YES"
          
    print("## ---------------------------------------------------------------------------------------")
    if ((ToDay == "YES") and (Now == "NO")):
       print("## RESULT: Found files uploaded for today, but they were not uploaded in last 10 minutes")
       print("## ---------------------------------------------------------------------------------------")

    if Now == "YES":
       print("## RESULT: Found Files uploaded for last 10 minutes")
       print("## ---------------------------------------------------------------------------------------")
       print("## validating if any ec2 instance created in last 60 minutes")
       create_instance()

    if ToDay == "NO":
       print("## RESULT: There is no files uploaded for today")
       print("## ---------------------------------------------------------------------------------------")
