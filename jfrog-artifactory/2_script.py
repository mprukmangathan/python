import boto3
import json
import time

def lambda_handler(event, context):
    ec2 = boto3.resource('ec2')
    ec2_client = boto3.client('ec2')
    s3 = boto3.client('s3')
    Mylist = []

    # Get the latest ec2 instance name from s3 bucket
    print("## Getting lastest instance name from S3")
    for key in s3.list_objects(Bucket='sandbox-ec2-hostname')['Contents']:
        Mylist.append(key['Key'])
    latest_instance_name = Mylist.pop()

    IsExist = "No"
    # Finding instance id from instance name
    print("## Validating the instance %s" %latest_instance_name)
    for instance in ec2.instances.all():
        if instance.state['Name'] == "running":
            for tag in instance.tags:
                if tag['Value'] == latest_instance_name:
                   ec2_instance_id = instance.id
                   IsExist = "Yes"
                   print("## Instance %s is up and running" %ec2_instance_id)
    
    # exit if there is no running instance
    if IsExist == "No":
       print("## Instance %s is not found or not running" %latest_instance_name)
       exit(1)

    # Initializing the system manager
    ssm_client = boto3.client('ssm', region_name="us-east-2")

    # Upgrading system manager agent on windows instance
    try:
        UpdateSSMAgent = ssm_client.send_command(InstanceIds=[ec2_instance_id],DocumentName="AWS-UpdateSSMAgent")
        print("## Updated the SSM agent on %s" %latest_instance_name)
    except:
        print("## Unable to perform UpdateSSMAgent operation")
        exit(1)

    # Running PS command on windows instance - count the number of files in metadata directory
    try:
        RunPowershellScript = ssm_client.send_command(
                                  InstanceIds=[ec2_instance_id],
                                  DocumentName="AWS-RunPowerShellScript",
                                  Parameters={ 'commands':[ 'cd C:\opt\metadata;(Get-ChildItem -File | Measure-Object).Count' ] }
                              )
        time.sleep(3)
        print("## Running powershell command on %s" %latest_instance_name)
    except:
        print("## Running RunPowershellScript failed")
        exit(1)

    # Getting command id from above step
    try:
        command_id = RunPowershellScript['Command']['CommandId']
        print("## Command id - %s" %command_id)
    except:
        print("## Issue finding command id from RunPowershellScript")
        exit(1)

    # Extracting output
    output = ssm_client.get_command_invocation(CommandId=command_id, InstanceId=ec2_instance_id)
    print("## Output status => %s, Output content => %s" %(output['StatusDetails'],output['StandardOutputContent'].rstrip()))
    
    # Stopping instance & taking image if the output is 8
    if output['StatusDetails'] == 'Success':
       if output['StandardOutputContent'].rstrip() == '8':
           print("## Found all 8 apps on server, Hence stopping the instance")
           try:
              print("## Stopping instance - %s" %latest_instance_name)
              ec2_client.stop_instances(InstanceIds=[ec2_instance_id], DryRun=False)
              status = ""
              while status != "stopped":
                    response = ec2_client.describe_instances(DryRun=False, InstanceIds =[ec2_instance_id])
                    status = response['Reservations'][0]['Instances'][0]['State']['Name']
              print("## Creating image from instance %s" %latest_instance_name)
              ec2_client.create_image(InstanceId=ec2_instance_id,Name=latest_instance_name,NoReboot=True)
              print("## Process Completed, exiting !")
           except:
              print("## Unable to stop the instance")   
       else:
           print("## Found only %s apps on server" %output['StandardOutputContent'].rstrip())
