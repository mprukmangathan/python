#!/usr/bin/python

import os
import requests
from bs4 import BeautifulSoup
import datetime
import boto3
import time

def create_ec2_instance(user_data, instance_name):
    client = boto3.client('ec2', region_name='us-east-2')
    ec2 = boto3.resource('ec2')
  
    response = client.run_instances(
       BlockDeviceMappings=[
           {
               'DeviceName': '/dev/xvda',
               'Ebs': {
                   'DeleteOnTermination': True,
                   'VolumeSize': 8,
                   'VolumeType': 'gp2'
               },  
           },  
       ],  
       ImageId='ami-0587770c46124f677',
       InstanceType='t2.micro',
       KeyName='ruk2020',
       MaxCount=1,
       MinCount=1,
       Monitoring={
          'Enabled': False
       },  
       SecurityGroupIds=[
          'sg-04a97a6bb4cc46013',
       ],  
       TagSpecifications=[
          { 
             'ResourceType': 'instance',
             'Tags': [
                {
                    'Key': 'Name',
                    'Value': instance_name
                },  
             ]  
          },   
       ],  
       UserData = user_data
    )   

    print ("Holding for a moment to let the ec2 instance finish the pre-deployment...")
    for x in range(5):
        for instance in ec2.instances.all():
            if instance.state['Name'] == "stopped":
               for tag in instance.tags:
                   if tag['Value'] == instance_name:
                      ec2_instance_id = instance.id
                      print ("Instance is stopped, Hence creating image from instance")
                      client.create_image(InstanceId=ec2_instance_id,Name=instance_name,NoReboot=True)
                      exit(0) 
            else:
               print ("instance %s is still running..." %instance_name)
        time.sleep(60)
    print ("instance %s is still running even after 5 minutes, there might be some issue with execution" %instance_name)


ec2_instance_name = "win-git-app"
udata = '''<script>
mkdir c:\gitrepo
mkdir c:\gitrepo\myrepo
mkdir c:\opt
cd c:\gitrepo

echo [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 > download_git.ps1
echo $down = New-Object System.Net.WebClient >> download_git.ps1
echo $url = 'https://bitbucket.org/mprukmangathan/powershell/get/82afe4343892.zip' >> download_git.ps1
echo $file = 'C:\gitrepo\myrepo.zip' >> download_git.ps1
echo $down.DownloadFile($url,$file) >> download_git.ps1
echo $exec = New-Object -com shell.application >> download_git.ps1
echo $destination = 'C:\gitrepo\myrepo'  >> download_git.ps1
echo Expand-Archive -LiteralPath $file -DestinationPath $destination >> download_git.ps1
echo exit >> download_git.ps1

powershell "& ""C:\gitrepo\download_git.ps1"""
powershell "& ""C:\gitrepo\myrepo\*\software\install.ps1"""

net session >nul 2>&1
if %errorLevel% == 0 (
    @echo on
    echo process successfull > execution_logs.txt
    shutdown /s
) else (
    @echo on
    echo process unsuccessfull  > execution_logs.txt
    @echo off
    exit /b 0
)
</script>'''

Current_Date = datetime.datetime.today().strftime ('%d-%b-%Y')
#print ('Current Date: ' + str(Current_Date))
now = datetime.datetime.now()
current_time = now.strftime("%H:%M")
#print("Current Time =", current_time)
Ten_minutes = datetime.timedelta(minutes=10)
Validate_time = now - Ten_minutes
tem_min_before = Validate_time.strftime("%H:%M")
#print ("ten minutes before =", tem_min_before)

ip = os.environ['elasticip']
url = "http://" + ip + ":8082/artifactory/generic-local/"
response = requests.get(url, auth=('admin','admin@123'))
soup = BeautifulSoup(response.text, 'html.parser')
Mylist = soup.find_all('pre')
Mylist.pop(0)

for element in Mylist:
    Mydata = str(element)
    Newlist = Mydata.split("\n")
    for item in Newlist:
        try:
            nums = item.split("</a>",1)[1]
            data = nums.split()
            #print (type(data[0]))
            if Current_Date == data[0]: 
               print ("date matches - %s-%s" %(data[0],data[1]))
               if ((data[1] >= tem_min_before) and (data[1] <= current_time)):
                  #print ("%s >= %s and %s <= %s" %(data[1],tem_min_before,data[1],current_time))
                  print ("Found files uploaded for last 10 minutes, Hence creating ec2 instance...")
                  create_ec2_instance(udata,ec2_instance_name)
               else:
                  print ("There is no files uploaded for last 10 minutes")
            else:
               print ("There is no files uploaded for today - %s-%s" %(data[0],data[1]))
        except IndexError:
            print ("")
